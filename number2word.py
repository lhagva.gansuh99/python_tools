import re

def number2word(number,is_fina=True):
    if check_phone_number(number):
        res = ''
        for i in range(4):
            tmp = str(number)[i*2:(i+1)*2]
            if tmp[0] == "0":
                res = res + " нойл " + _last_digit_2_str(tmp[1]) + " "
            else:
                res = res +" " +  _2_digits_2_str(tmp,is_fina)
    return res


    digit_name = {1: '', 2: 'мянга', 3: 'сая', 4: 'тэрбум', 5: 'их наяд', 6: 'тунамал'}
    floating_point_names = {1: ' аравны ', 2: ' зууны ', 3: ' мянганы '}

    if re.match(r'[0-9]+[.][0-9]+', number) is not None:

        whole = number.split('.')[0]
        fraction = number.split('.')[1]
        fraction_len = len(fraction)

        if fraction_len < 4:
            floating_point_name = floating_point_names[fraction_len]
        else:
            floating_point_name = floating_point_names[3]

        return number2word(whole) + floating_point_name + number2word(fraction,is_fina)

    else:
        digit_len = len(number)


    if digit_len == 1:
        return _last_digit_2_str(number) if is_fina else _1_digit_2_str(number)
    if digit_len == 2:
        return _2_digits_2_str(number,is_fina)
    if digit_len == 3:
        return _3_digits_to_str(number,is_fina)
    if digit_len < 7:

        thousand = 'мянган' if len(number[-3:].lstrip('0')) == 0 and not is_fina else 'мянга'

        return _3_digits_to_str(number[:-3], False) + ' ' + thousand + ' ' + _3_digits_to_str(number[-3:],is_fina)

    digitgroup = [number[0 if i - 3 < 0 else i - 3:i] for i in reversed(range(len(number), 0, -3))]
    count = len(digitgroup)
    i = 0
    result = ''
    while i < count - 1:
        result += ' ' + (_3_digits_to_str(digitgroup[i], False) + ' ' + digit_name[count - i])
        i += 1
    return result.strip() + ' ' + _3_digits_to_str(digitgroup[-1])



def _1_digit_2_str(digit):
    return {'0': '', '1': 'нэг', '2': 'хоёр', '3': 'гурван', '4': 'дөрвөн', '5': 'таван', '6': 'зургаан',
            '7': 'долоон', '8': 'найман', '9': 'есөн'}[digit]


def _last_digit_2_str(digit):
    return {'0': 'тэг', '1': 'нэг', '2': 'хоёр', '3': 'гурав', '4': 'дөрөв', '5': 'тав', '6': 'зургаа', '7': 'долоо',
            '8': 'найм', '9': 'ес'}[digit]


def _2_digits_2_str(digit, is_fina=True):
    word2 = {'0': '', '1': 'арван', '2': 'хорин', '3': 'гучин', '4': 'дөчин', '5': 'тавин', '6': 'жаран', '7': 'далан',
             '8': 'наян', '9': 'ерэн'}
    word2fina = {'10': 'арав', '20': 'хорь', '30': 'гуч', '40': 'дөч', '50': 'тавь', '60': 'жар', '70': 'дал',
                 '80': 'ная', '90': 'ер'}
    if digit[1] == '0':
        return word2fina[digit] if is_fina else word2[digit[0]]
    digit1 = _last_digit_2_str(digit[1]) if is_fina else _1_digit_2_str(digit[1])
    return (word2[digit[0]] + ' ' + digit1).strip()


def _3_digits_to_str(digit, is_fina=True):
    digstr = digit.lstrip('0')
    if len(digstr) == 0:
        return ''
    if len(digstr) == 1:
        # return _1_digit_2_str(digstr)
        return _last_digit_2_str(digstr) if is_fina else _1_digit_2_str(digstr)
    if len(digstr) == 2:
        return _2_digits_2_str(digstr, is_fina)
    if digit[-2:] == '00':
        return _1_digit_2_str(digit[0]) + ' зуу' if is_fina else _1_digit_2_str(digit[0]) + ' зуун'
    else:
        return _1_digit_2_str(digit[0]) + ' зуун ' + _2_digits_2_str(digit[-2:], is_fina)
def check_phone_number(number):
    # Түгээмэл дугаарлалтууд
    arr = ["70", "71", "72", "75", "76", "77", "78", "79", "80", "83", "85", "86", "88", "89", "90", "91", "92", "93", "94", "95", "96", "97", "98", "99"]
    if len(number) != 8:
        return False
    tmp = str(number)[:2]
    if tmp in arr:
        return True
    return False
def numbers2words(sentence):

    units = ['доллар','хувь','хувийг','он','онд','төгрөг','жил','өдөр','сард','байлдагч','километр','км','сая','см','пенс','тэрбум', 'сая', 'кг','сарын']
    units = ['доллар','хувь','хувийг','он','онд','төгрөг','жил','өдөр','сард','сар','байлдагч','километр','км','сая','см','пенс','тэрбум', 'сая', 'кг']

    for unit in units:
        p = re.compile(rf"([0-9]+[.]?[0-9]*) {unit}")
        numbers = p.findall(sentence)
        # print('numbers',numbers)
        for number in numbers:
            word = number2word(number,False)
            sentence = re.sub(number + ' ' + unit, word + ' ' + unit, sentence)

    words = sentence.strip().lower().split()
    new_words = []
    # print(words)
    for word in words:
        # print(word)
        # print(word)
        if re.match(r'^-?\d+(?:\.\d+)?$', word) is not None:
            word = number2word(word)

        word = re.sub(r'[^\w\s]', '', word)
        new_words.append(word)

    return romans2words(" ".join(new_words) + ".")


def roman2int(s):
    """
    :type s: str
    :rtype: int
    """
    roman = {'i': 1, 'v': 5, 'x': 10, 'l': 50, 'c': 100, 'd': 500, 'm': 1000, 'iv': 4, 'ix': 9, 'xl': 40,
             'xc': 90, 'cd': 400, 'cm': 900}
    i = 0
    num = 0
    while i < len(s):
        if i + 1 < len(s) and s[i:i + 2] in roman:
            num += roman[s[i:i + 2]]
            i += 2
        else:
            # print(i)
            num += roman[s[i]]
            i += 1
    return num

def romans2words(sentence):
    regex = '^(?=[mdclxvi])m*(c[md]|d?c*)(x[cl]|l?x*)(i[xv]|v?i*)$'

    sentence = re.sub("хх", "xx", sentence)
    # sentence.re
    # regex = '^(?=[MDCLXVI])M*(C[MD]|D?C*)(X[CL]|L?X*)(I[XV]|V?I*)$'
    words = sentence.strip().lower().split()

    new_words = []

    for word in words:
        if re.match(regex, word):
            new_words.append(number2word(str(roman2int(word))) + " дугаар")
        else:
            new_words.append(word)

    return " ".join(new_words)

# sentence = romans2words("түүнээс ялгаатай нь xviii зууны иммануэл кант xx хх зууны жон ролс зэрэг орчин үеийн улс төрийн философичид хүний.",)
# print(sentence)
# number = '2004'
# print(number[:-3])
# word = number2word(number)
# print(word)

# words = numbers2words("түүнээс ялгаатай нь xviii зууны иммануэл кант xx хх зууны жон ролс зэрэг орчин үеийн улс төрийн философичид хүний.")
words = numbers2words("миний дугаар 95910726 .")

print(words)
# numbers2words('-------')

